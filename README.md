<b>Validation:</b>
- Guard.ValidateInstance(instance);
- Guard.ValidateStatic(typeof(StaticClass));

<b>Available guards:</b>
- EqualTo:<br/><br/>
[Guard(EqualTo = 4)]<br/>
public int Property { get; set; }<br/><br/>
- GreaterOrEqual:<br/><br/>
[Guard(GreaterOrEqual = 6.0)]<br/>
public double Property { get; set; }<br/><br/>
- GreaterThan:<br/><br/>
[Guard(GreaterThan = 7.0f)]<br/>
public float Property { get; set; }<br/><br/>
- LessOrEqual:<br/><br/>
[Guard(LessOrEqual = 4)]<br/>
public int Property { get; set; }<br/><br/>
- LessThan:<br/><br/>
[Guard(LessThan = 2.0)]<br/>
public double Property { get; set; }<br/><br/>
- NotEqualTo:<br/><br/>
[Guard(NotEqualTo = "Foo")]<br/>
public string Property { get; set; }<br/><br/>
- NotNull:<br/><br/>
[Guard(NotNull = true)]<br/>
public object Property { get; set; }<br/><br/>
- NotNullOrEmpty:<br/><br/>
[Guard(NotNullOrEmpty = true)]<br/>
public string Property { get; set; }<br/><br/>
- NotTypeOf:<br/><br/>
[Guard(NotTypeOf = typeof(string))]<br/>
public object Property { get; set; }<br/><br/>
- TypeOf:<br/><br/>
[Guard(TypeOf = typeof(Example)]<br/>
public object Property { get; set; }<br/><br/>