﻿using PropertyGuard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            var example = new Example()
            {
                Obj = 6.0
            };

            try
            {
                Guard.ValidateInstance(example);
                Console.WriteLine("No exception has been handled.");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            Console.ReadKey();
        }
    }

    internal class Example
    {
        [Guard(NotTypeOf = typeof(int))]
        public object Obj { get; set; }
    }
}