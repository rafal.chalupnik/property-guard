﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace PropertyGuard
{
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class Guard : Attribute
    {
        private static void CheckProperties(PropertyInfo[] _properties, object _object)
        {
            var stringBuilder = new StringBuilder();

            foreach (var property in _properties)
            {
                var customAttributes = property.GetCustomAttributes(true);
                foreach (var attribute in customAttributes)
                {
                    if (attribute is Guard guard)
                    {
                        var name = property.Name;
                        var value = property.GetValue(_object);

                        var checkers = new List<Func<string, object, string>>()
                        {
                            guard.CheckEqualTo,
                            guard.CheckGreaterOrEqual,
                            guard.CheckGreaterThan,
                            guard.CheckLessOrEqual,
                            guard.CheckLessThan,
                            guard.CheckNotEqualTo,
                            guard.CheckNotNull,
                            guard.CheckNotNullOrEmpty,
                            guard.CheckNotTypeOf,
                            guard.CheckTypeOf
                        };
                        string result;

                        foreach (var checker in checkers)
                        {
                            result = checker(name, value);
                            if (result != null) stringBuilder.AppendLine(result);
                        }
                    }
                }
            }

            var output = stringBuilder.ToString();
            if (output != string.Empty)
                throw new GuardException(output);
        }

        private static string ConstructCannotCheckConstraintMessage(string _constraintName, string _propertyName)
        {
            return $"Cannot check {_constraintName} constraint for null {_propertyName} property value.";
        }

        private static string ConstructConstraintTypeMessage(string _propertyName, Type _type)
        {
            return $"Constraint must be the same type as {_propertyName} property ({_type}).";
        }

        private string CheckEqualTo<T>(string _name, T _value)
        {
            if (EqualTo == null) return null;
            if (_value == null) return ConstructCannotCheckConstraintMessage("EqualTo", _name);
            if (EqualTo.GetType() != _value.GetType()) return ConstructConstraintTypeMessage(_name, _value.GetType());

            var castedEqualTo = (T)EqualTo;
            return Comparer<T>.Default.Compare(_value, castedEqualTo) != 0
                ? $"{_name} property is not equal to {castedEqualTo} as expected."
                : null;
        }

        private string CheckGreaterOrEqual<T>(string _name, T _value)
        {
            if (GreaterOrEqual == null) return null;
            if (_value == null) return ConstructCannotCheckConstraintMessage("GreaterOrEqual", _name);
            if (GreaterOrEqual.GetType() != _value.GetType())
                return ConstructConstraintTypeMessage(_name, _value.GetType());

            var castedGreaterOrEqual = (T)GreaterOrEqual;
            return Comparer<T>.Default.Compare(_value, castedGreaterOrEqual) < 0
                ? $"{_name} property is not greater or equal {castedGreaterOrEqual} as expected."
                : null;
        }

        private string CheckGreaterThan<T>(string _name, T _value)
        {
            if (GreaterThan == null) return null;
            if (_value == null) return ConstructCannotCheckConstraintMessage("GreaterThan", _name);
            if (GreaterThan.GetType() != _value.GetType())
                return ConstructConstraintTypeMessage(_name, _value.GetType());

            var castedGreaterThan = (T)GreaterThan;
            return Comparer<T>.Default.Compare(_value, castedGreaterThan) <= 0
                ? $"{_name} property is not greater than {castedGreaterThan} as expected."
                : null;
        }

        private string CheckLessOrEqual<T>(string _name, T _value)
        {
            if (LessOrEqual == null) return null;
            if (_value == null) return ConstructCannotCheckConstraintMessage("LessOrEqual", _name);
            if (LessOrEqual.GetType() != _value.GetType())
                return ConstructConstraintTypeMessage(_name, _value.GetType());

            var castedLessOrEqual = (T)LessOrEqual;
            return Comparer<T>.Default.Compare(_value, castedLessOrEqual) > 0
                ? $"{_name} property is not less or equal {castedLessOrEqual} as expected."
                : null;
        }

        private string CheckLessThan<T>(string _name, T _value)
        {
            if (LessThan == null) return null;
            if (_value == null) return ConstructCannotCheckConstraintMessage("LessThan", _name);
            if (LessThan.GetType() != _value.GetType())
                return ConstructConstraintTypeMessage(_name, _value.GetType());

            var castedLessThan = (T)LessThan;
            return Comparer<T>.Default.Compare(_value, castedLessThan) >= 0
                ? $"{_name} property is not less than {castedLessThan} as expected."
                : null;
        }

        private string CheckNotEqualTo<T>(string _name, T _value)
        {
            if (NotEqualTo == null) return null;
            if (_value == null) return ConstructCannotCheckConstraintMessage("NotEqualTo", _name);
            if (NotEqualTo.GetType() != _value.GetType())
                return ConstructConstraintTypeMessage(_name, _value.GetType());

            var castedNotEqualTo = (T)NotEqualTo;
            return Comparer<T>.Default.Compare(_value, castedNotEqualTo) == 0
                ? $"{_name} property is equal to {castedNotEqualTo}, expected not."
                : null;
        }

        private string CheckNotNull<T>(string _name, T _value)
        {
            if (!NotNull) return null;
            return _value == null ? $"{_name} property is null, expected not." : null;
        }

        private string CheckNotNullOrEmpty<T>(string _name, T _value)
        {
            if (!NotNullOrEmpty) return null;
            if (_value == null) return $"{_name} property is null, expected not.";
            if (_value.GetType() != typeof(string)) return ConstructConstraintTypeMessage(_name, _value.GetType());
            return string.IsNullOrEmpty(Convert.ToString(_value)) ? $"{_name} property is empty, expected not." : null;
        }

        private string CheckNotTypeOf<T>(string _name, T _value)
        {
            if (NotTypeOf == null || _value == null) return null;
            return _value.GetType() == NotTypeOf
                ? $"{_name} property is type of {NotTypeOf}, expected not."
                : null;
        }

        private string CheckTypeOf<T>(string _name, T _value)
        {
            if (TypeOf == null || _value == null) return null;
            return _value.GetType() != TypeOf
                ? $"{_name} property is not type of {TypeOf} as expected."
                : null;
        }

        public object EqualTo { get; set; }
        public object GreaterOrEqual { get; set; }
        public object GreaterThan { get; set; }
        public object LessOrEqual { get; set; }
        public object LessThan { get; set; }
        public object NotEqualTo { get; set; }
        public bool NotNull { get; set; }
        public bool NotNullOrEmpty { get; set; }
        public Type NotTypeOf { get; set; }
        public Type TypeOf { get; set; }

        public static void ValidateInstance(object _object)
        {
            if (_object != null)
                CheckProperties(_object.GetType().GetProperties(), _object);
        }

        public static void ValidateStatic(Type _type)
        {
            if (_type != null)
                CheckProperties(_type.GetProperties(), null);
        }
    }
}