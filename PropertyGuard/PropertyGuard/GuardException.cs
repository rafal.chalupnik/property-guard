﻿using System;

namespace PropertyGuard
{
    public class GuardException : Exception
    {
        public GuardException(string _message) : base(_message)
        {
        }
    }
}