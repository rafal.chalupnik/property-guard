﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("PropertyGuard")]
[assembly: AssemblyDescription("Simple property value checker for .NET")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Rafał Chałupnik")]
[assembly: AssemblyProduct("PropertyGuard")]
[assembly: AssemblyCopyright("Copyright © Rafał Chałupnik 2017")]
[assembly: AssemblyTrademark("PropertyGuard")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(true)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("1b2e2a64-6075-455c-a677-3d7c527b10ae")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("0.1.0.0")]
[assembly: AssemblyFileVersion("0.1.0.0")]
