﻿using System;
using NUnit.Framework;

namespace PropertyGuard.Tests.Unit
{
    [TestFixture]
    public class GuardTests
    {
        private Example example_;

        [Test]
        public void EqualTo_Correct()
        {
            Guard.ValidateInstance(example_);
            Assert.Pass();
        }

        [Test]
        public void EqualTo_IncompatibleType()
        {
            example_.EqualTo = new byte();
            try
            {
                Guard.ValidateInstance(example_);
                Assert.Fail();
            }
            catch (GuardException)
            {
                Assert.Pass();
                example_.EqualTo = 4;
            }
        }

        [Test]
        public void EqualTo_Incorrect()
        {
            example_.EqualTo = 6;
            try
            {
                Guard.ValidateInstance(example_);
                Assert.Fail();
            }
            catch (GuardException)
            {
                Assert.Pass();
                example_.EqualTo = 4;
            }
        }

        [Test]
        public void EqualTo_NullExpected()
        {
            try
            {
                Guard.ValidateInstance(example_);
                Assert.Fail();
            }
            catch (GuardException)
            {
                Assert.Pass();
                example_.EqualTo = 4;
            }
        }

        [SetUp]
        public void SetUp()
        {
            example_ = new Example();
        }
    }
}