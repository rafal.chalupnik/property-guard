﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PropertyGuard.Tests.Unit
{
    internal class Example
    {
        [Guard(EqualTo = 4)]
        public int EqualTo { get; set; }

        public Example()
        {
            EqualTo = 4;
        }
    }
}